# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Svetoslav Stefanov <svetlisashkov@yahoo.com>, 2014.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: powerdevilactivitiesconfig\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-21 02:30+0000\n"
"PO-Revision-Date: 2022-08-25 20:40+0200\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Lokalize 22.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Светослав Стефанов"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "svetlisashkov@yahoo.com"

#: activitypage.cpp:59
#, kde-format
msgid ""
"The activity service is running with bare functionalities.\n"
"Names and icons of the activities might not be available."
msgstr ""
"Услугата за дейности се изпълнява с минимална функционалност.\n"
"Имената и иконите на дейностите може да не са налични."

#: activitypage.cpp:132
#, kde-format
msgid ""
"The activity service is not running.\n"
"It is necessary to have the activity manager running to configure activity-"
"specific power management behavior."
msgstr ""
"Услугата за дейности не се изпълнява.\n"
"За да може да се настройва поведението на енергоспестяването за всяка една "
"дейност, мениджъра за дейности трябва да е стартиран."

#: activitypage.cpp:227
#, kde-format
msgid "The Power Management Service appears not to be running."
msgstr "Изглежда, че услугата за управление на захранването не работи."

#: activitywidget.cpp:98
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Приспиване"

#: activitywidget.cpp:102
#, kde-format
msgid "Hibernate"
msgstr "Хибернация"

#: activitywidget.cpp:104
#, kde-format
msgid "Shut down"
msgstr "Изключване"

#: activitywidget.cpp:108
#, kde-format
msgid "PC running on AC power"
msgstr "Компютърът работи на захранване от ел. мрежа"

#: activitywidget.cpp:109
#, kde-format
msgid "PC running on battery power"
msgstr "Компютърът работи на захранване от батерията"

#: activitywidget.cpp:110
#, kde-format
msgid "PC running on low battery"
msgstr "Компютърът работи с ниско ниво на батерията"

#: activitywidget.cpp:139
#, kde-format
msgctxt "This is meant to be: Act like activity %1"
msgid "Activity \"%1\""
msgstr "Дейност \"%1\""

#. i18n: ectx: property (text), widget (QRadioButton, noSettingsRadio)
#: activityWidget.ui:19
#, kde-format
msgid "Do not use special settings"
msgstr "Без използване на специални настройки"

#. i18n: ectx: property (text), widget (QRadioButton, actLikeRadio)
#: activityWidget.ui:31
#, kde-format
msgid "Act like"
msgstr "Действие като"

#. i18n: ectx: property (text), widget (QRadioButton, specialBehaviorRadio)
#: activityWidget.ui:60
#, kde-format
msgid "Define a special behavior"
msgstr "Задаване на специално поведение"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownScreenBox)
#: activityWidget.ui:72
#, kde-format
msgid "Never turn off the screen"
msgstr "Екранът никога да не се изключва"

#. i18n: ectx: property (text), widget (QCheckBox, noShutdownPCBox)
#: activityWidget.ui:79
#, kde-format
msgid "Never shut down the computer or let it go to sleep"
msgstr "Компютърът никога да не се изключва и приспива"

#. i18n: ectx: property (text), widget (QCheckBox, alwaysBox)
#: activityWidget.ui:91
#, kde-format
msgid "Always"
msgstr "Винаги"

#. i18n: ectx: property (text), widget (QLabel, alwaysAfterLabel)
#: activityWidget.ui:101
#, kde-format
msgid "after"
msgstr "след"

#. i18n: ectx: property (suffix), widget (QSpinBox, alwaysAfterSpin)
#: activityWidget.ui:108
#, kde-format
msgid " min"
msgstr "мин"

#. i18n: ectx: property (text), widget (QRadioButton, separateSettingsRadio)
#: activityWidget.ui:138
#, kde-format
msgid "Use separate settings"
msgstr "Използване на отделни настройки"
